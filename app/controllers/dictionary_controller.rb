class DictionaryController < ApplicationController
	include DictionaryHelper
	
	def index
		
		@url = params[:path_to_file]
		render json: { status: 404, message: "Please give correct path of file in path_to_file variable" } and return unless @url.present?
	    
	    @words_arr = read_file_from_url
	    render json: { status: 404, message: "FILE_NOT_FOUND" } and return unless @words_arr.present?
		
		words_arr = @words_arr.sort
		@words_hash = create_word_hash
		
		combinations = fetch_matching_words
		
        render json: { status: 200, data: combinations }
    end
end
