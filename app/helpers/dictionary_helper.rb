module DictionaryHelper
	require 'open-uri'


	def read_file_from_url
		begin
			words_arr = []
		    open(@url) do |f|
			    f.each_line do |line|
			      line = line.strip
			   	  words_arr << line.downcase if line.present?
			    end
			end
			words_arr
		rescue => e
	       case e
	       when OpenURI::HTTPError
	       	  []
	       end
	     end  
	 end


	 def create_word_hash
	 	words_hash = {}
	 	@words_arr.each_with_index do |ele, index|
			words_hash[ele] = index
		end
		words_hash
	 end

	 def fetch_matching_words
	 	combinations = []
	 	@words_hash.each do |word,  ind|
		   key_index = ind + 1
	       sub_key_hsh = {}
	       if @words_arr[key_index].present?
			   while @words_arr[key_index].include? word
			   	    sub_key_hsh[@words_arr[key_index].gsub(word, "")] = @words_arr[key_index];
			        key_index += 1
			        if !@words_arr[key_index].present?
			        	break
			        end
			   end
			   sub_key_hsh.each do |word1, main_word|							   	    
			   	    combinations << "#{word} + #{word1} = #{main_word}" if @words_hash[word1].present?
			   end
			end
		end
		combinations
	 end
	    
end
